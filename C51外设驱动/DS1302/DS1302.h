#ifndef DS1302_H
#define DS1302_H
	sbit DS1302_SCK	= P2^4;
	sbit DS1302_SIO = P2^5;
	sbit DS1302_RST = P2^0;
    
    
void writeByte(void);
void readByte(void);
void setChargePrmt(void);
void closeWP(void);
void openWP(void);
void write(uint8 adr,uint8 dat);
uint8 read(uint8 adr);
void DS1302_getTime(uint8 *buf);
void DS1302_setTime(uint8 hour,uint8 minute,uint8 second);
void DS1302_init(void);

#endif





























#endif