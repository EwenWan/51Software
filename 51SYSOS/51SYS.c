#include <reg52.h>
#include <51Kernel.h>
#include <usart.h>
extern unsigned char idata os_task_stack[MAX_TASKS][20];
extern bit           read_flag;
extern unsigned char inbuf1[INBUF_LEN];
void task_0(void)
{
  while(1)
  {

  }
}
void task_1(void)
{
  while(1)
  {
     if(read_flag)  //如果取数标志已置位，就将读到的数从串口发出 
      {
       read_flag=0; //取数标志清0 
       send_string_com(inbuf1,INBUF_LEN);
      }
			delay_ms(1000);
  }
}

void task_2(void)
{
	int i;
  while(1)
  {
		i++;
		os_delay(7);
  }
}

void task_3(void)
{

  while(1)
  {

  }
}

void task_4(void)
{
  while(1)
  {

  }
}

void main(void)
{
  os_init();
  init_serialcomm();
  os_task_create(4,(unsigned int)&task_0,(unsigned char)os_task_stack[4]);
  os_task_create(3,(unsigned int)&task_1,(unsigned char)os_task_stack[3]);
  os_task_create(2,(unsigned int)&task_2,(unsigned char)os_task_stack[2]);
  os_task_create(1,(unsigned int)&task_3,(unsigned char)os_task_stack[1]);
  os_task_create(0,(unsigned int)&task_4,(unsigned char)os_task_stack[0]);
  os_start();
}