#ifndef __51KERNEL_H 
#define __51KERNEL_H
#include <reg52.h>
#define MAX_TASKS 5

extern volatile unsigned char int_count;
extern volatile unsigned char os_en_cr_count;

#define enter_int() EA=0;int_count++;
#define os_enter_critical() EA=0;os_en_cr_count++;
#define os_exit_critical() if(os_en_cr_count>=1){os_en_cr_count--;if(os_en_cr_count==0)EA=1;}

typedef struct os_task_control_table
{
  unsigned char os_task_wait_tick;
  unsigned char os_task_stack_top;
} TCB;

void os_init(void);
void os_task_create(unsigned char task_id ,unsigned int task_point,unsigned char stack_point);
void os_delay(unsigned char ticks);
void os_start(void);
void os_task_switch(void);
void exit_int(void);
void delay_ms(unsigned int xms);
#endif